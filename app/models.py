from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Escolaridad(models.Model):
    escolaridad=models.CharField(max_length=100)
    def __unicode__(self):
        return self.escolaridad

class Tag(models.Model):
    tag=models.CharField(max_length=100)
    def __unicode__(self):
        return self.tag
'''
class Aspirante(models.Model):
    correo=models.EmailField()
    nombre=models.CharField(max_length=255)
    direccion=models.TextField()
    telefono=models.CharField(max_length=100)
    curriculum=models.FileField(upload_to='curriculums')
    ciudad=models.CharField(max_length=100)
    calificacion=models.CharField(max_length=3)
    Escolaridad=models.ForeignKey(Escolaridad)
    def __unicode__(self):
        return self.correo

class AspiranteTags(models.Model):
    Aspirante=models.ForeignKey(Aspirante)
    Tag=models.ForeignKey(Tag)

class Solicitud(models.Model):
    Aspirante=models.ForeignKey(Aspirante)
    Vacante=models.ForeignKey(Vacante)
    fecha=models.DateTimeField(auto_now_add=True,editable=False)
    usuario=models.ForeignKey(User)

class Contratos(models.Model):
    Vacante=models.ForeignKey(Vacante)
    Solicitud=models.ForeignKey(Solicitud)
    fecha=models.DateTimeField(auto_now_add=True,editable=False)
    usuario=models.ForeignKey(User)
'''
