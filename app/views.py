from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from contratos.models import Contrato
from vacantes.models import Vacante
# Create your views here.

class Home(TemplateView):
	template_name = "home.html"

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(Home, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)
		contratos=Contrato.objects.all()
		context['totalvacantes']=Vacante.objects.exclude(contrato__in=contratos).count()
		context['totalcontratos']=contratos.count()
		return context
