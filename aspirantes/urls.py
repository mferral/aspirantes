from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from usuarios.views import LoginView,LogoutView
from app.views import Home
from personas.views import Personas
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^index/', TemplateView.as_view(template_name="index.html")),
    url(r'', include('vacantes.urls')),
    url(r'', include('solicitudes.urls')),
    url(r'', include('contratos.urls')),
    url(r'^aspirantes/', Personas.as_view()),
    url(r'^login/$', LoginView.as_view(), name='Login'),
    url(r'^$', Home.as_view()),
    url(r'^salir/$', LogoutView.as_view(),name='Salir'),
    #url(r'^salir/$', 'django.contrib.auth.views.logout',{'next_page': '/login/'},name='Salir'),
    #url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
    #        'document_root': settings.STATIC_ROOT,
    #}),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
