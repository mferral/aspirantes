from django.conf.urls import patterns, url
from .views import Vacantes,VacanteCreate,VacanteUpdate,VacanteDelete
from django.contrib.auth.decorators import login_required

urlpatterns = [
	url(r'^vacantes/$', login_required(Vacantes.as_view()), name='vacantes'),
	#url(r'^albums/(?P<titulo>[\w\-]+)/$', login_required(AlbumListView.as_view()), name='albums'),
	url(r'^vacante/$', login_required(VacanteCreate.as_view()), name='vacante'),
	url(r'^vacante_edit/(?P<pk>\d+)$', login_required(VacanteUpdate.as_view()), name='vacante_edit'),
	url(r'^vacante_delete/(?P<pk>\d+)$', login_required(VacanteDelete.as_view()), name='vacante_delete'),
]
