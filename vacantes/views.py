from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic import ListView
from .forms import VacanteForm
from .models import Vacante
from contratos.models import Contrato
from django.http import JsonResponse

# Create your views here.

class Vacantes(ListView):
	template_name = "vacantes.html"
	model = Vacante
	context_object_name = "vacantes"

	def dispatch(self, *args, **kwargs):
		return super(Vacantes, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Vacantes, self).get_context_data(**kwargs)
		return context

	def get_queryset(self):
		contratos=Contrato.objects.all()
		queryset=super(Vacantes,self).get_queryset().exclude(contrato__in=contratos).order_by('fecha')
		return queryset

class VacanteCreate(CreateView):
	form_class = VacanteForm
	template_name = 'vacante.html'
	success_url = '/vacantes/'

	def form_valid(self, form):
		obj = form.save(commit=False)
		obj.usuario = self.request.user
		return super(VacanteCreate, self).form_valid(form)

class VacanteUpdate(UpdateView):
	model = Vacante
	form_class = VacanteForm
	template_name = 'vacante.html'
	success_url = '/vacantes/'

class VacanteDelete(DeleteView):
	model = Vacante
	success_url = '/vacantes/'

	def dispatch(self, *args, **kwargs):
		resp = super(VacanteDelete, self).dispatch(*args, **kwargs)
		if self.request.is_ajax():
			response_data = {"result": "ok"}
			return JsonResponse(response_data)
		else:
			return resp
