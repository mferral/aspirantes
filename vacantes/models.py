from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Vacante(models.Model):
    titulo=models.CharField(max_length=255)
    descripcion=models.TextField()
    fecha=models.DateTimeField(auto_now_add=True,editable=False)
    estado=models.BooleanField(default=False)
    usuario=models.ForeignKey('auth.User',editable=False)

    def __unicode__(self):
        return self.titulo
