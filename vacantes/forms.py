from django import forms
from .models import Vacante
from django.forms import Textarea

class VacanteForm(forms.ModelForm):
    class Meta:
        model = Vacante
        fields = "__all__"
        widgets={
            "titulo":forms.TextInput(attrs={'placeholder':'Escriba el titulo','required':'True','class':'form-control fg-input'}),
            "descripcion":forms.Textarea(attrs={'placeholder':'Escriba la descripcion del empleo','required':'True','rows':'5','class':'form-control'}),
        }
