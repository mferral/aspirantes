from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

# Create your views here.

class Personas(TemplateView):
	template_name = "personas.html"

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(Personas, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Personas, self).get_context_data(**kwargs)
		return context
