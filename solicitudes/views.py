from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,DeleteView
from django.views.generic import ListView
from .forms import SolicitudForm
from .models import Solicitud
from vacantes.models import Vacante
from django.http import JsonResponse

# Create your views here.

class Solicitudes(ListView):
	template_name = "solicitudes.html"
	model = Solicitud
	context_object_name = "solicitudes"

	def dispatch(self, *args, **kwargs):
		return super(Solicitudes, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Solicitudes, self).get_context_data(**kwargs)
		vacante=Vacante.objects.get(pk=self.kwargs['pk'])
		context['titulo']=vacante.titulo
		return context

	def get_queryset(self):
		queryset=super(Solicitudes,self).get_queryset().filter(vacante=self.kwargs['pk']).order_by('fecha')
		return queryset


class SolicitudCreate(CreateView):
	form_class = SolicitudForm
	template_name = 'solicitud.html'
	success_url = '/confirmacion/'

	def form_valid(self, form):
		obj = form.save(commit=False)
		obj.usuario = self.request.user
		return super(SolicitudCreate, self).form_valid(form)

class SolicitudDelete(DeleteView):
	model = Solicitud
	success_url = '/'

	def dispatch(self, *args, **kwargs):
		resp = super(SolicitudDelete, self).dispatch(*args, **kwargs)
		if self.request.is_ajax():
			response_data = {"result": "ok"}
			return JsonResponse(response_data)
		else:
			return resp
