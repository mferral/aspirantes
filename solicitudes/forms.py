from django import forms
from .models import Solicitud
from django.forms import Textarea, FileInput
from app.models import Escolaridad
from vacantes.models import Vacante
from contratos.models import Contrato

class SolicitudForm(forms.ModelForm):
    class Meta:
        model = Solicitud
        fields = "__all__"
        widgets={
            "correo":forms.TextInput(attrs={'required':'True','placeholder':'Correo Electronico','class':'form-control'}),
            "nombre":forms.TextInput(attrs={'required':'True','placeholder':'Nombre Completo','class':'form-control'}),
            "direccion":forms.TextInput(attrs={'required':'True','placeholder':'Direccion','class':'form-control'}),
            "ciudad":forms.TextInput(attrs={'required':'True','placeholder':'Ciudad','class':'form-control'}),
            "telefono":forms.TextInput(attrs={'required':'True','placeholder':'Telefono(s)','class':'form-control'}),
            "curriculum":forms.FileInput(attrs={'required':'True'}),
            "escolaridad":forms.Select(attrs={'class':'form-control'}),
            "vacante":forms.Select(attrs={'class':'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(SolicitudForm, self).__init__(*args, **kwargs)
        self.fields['escolaridad'].empty_label = None
        self.fields['escolaridad'].queryset = Escolaridad.objects.order_by('id')
        self.fields['vacante'].empty_label = None
        contratos=Contrato.objects.all()
        self.fields['vacante'].queryset = Vacante.objects.exclude(contrato__in=contratos).order_by('id')
