from django.db import models
from app.models import Escolaridad
from vacantes.models import Vacante
# Create your models here.
class Solicitud(models.Model):
    correo=models.EmailField()
    nombre=models.CharField(max_length=255)
    direccion=models.TextField()
    telefono=models.CharField(max_length=100)
    ciudad=models.CharField(max_length=100)
    curriculum=models.FileField(upload_to='curriculums')
    fecha=models.DateTimeField(auto_now_add=True,editable=False)
    escolaridad=models.ForeignKey(Escolaridad)
    vacante=models.ForeignKey(Vacante)

    def __unicode__(self):
        return self.correo
