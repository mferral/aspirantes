from django.conf.urls import patterns, url
from .views import Solicitudes,SolicitudCreate,SolicitudDelete
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

urlpatterns = [
	url(r'^solicitudes/(?P<pk>\d+)$', login_required(Solicitudes.as_view()), name='solicitudes'),
	url(r'^solicitud/$', SolicitudCreate.as_view(), name='solicitudes'),
	url(r'^confirmacion/$', TemplateView.as_view(template_name="confirmacion.html"), name='confirmacion'),
	#url(r'^solicitud_edit/(?P<pk>\d+)$', login_required(VacanteUpdate.as_view()), name='vacante_edit'),
	url(r'^solicitud_delete/(?P<pk>\d+)$', login_required(SolicitudDelete.as_view()), name='solicitud_delete'),
]
