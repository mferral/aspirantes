from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from .models import Contrato
from django.views.generic import ListView
from django.shortcuts import redirect
# Create your views here.

class Contratos(ListView):
	template_name = "contratos.html"
	model = Contrato
	context_object_name = "contratos"

	def dispatch(self, *args, **kwargs):
		return super(Contratos, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Contratos, self).get_context_data(**kwargs)
		return context

@login_required
def contratocreate(request):
	usuario = request.user
	if request.POST:
		Contrato.objects.create(vacante_id=request.POST['id_vacante'],solicitud_id=request.POST['id_solicitud'],usuario=usuario)
	return redirect('/contratos/');
