from django.db import models
from vacantes.models import Vacante
from solicitudes.models import Solicitud
from django.contrib.auth.models import User
# Create your models here.
class Contrato(models.Model):
    vacante=models.ForeignKey(Vacante)
    solicitud=models.ForeignKey(Solicitud)
    fecha=models.DateTimeField(auto_now_add=True,editable=False)
    usuario=models.ForeignKey('auth.User',editable=False)

    def __unicode__(self):
        return self.vacante.titulo
