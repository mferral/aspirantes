from django.conf.urls import patterns, url
from .views import Contratos
from django.contrib.auth.decorators import login_required
from contratos.views import contratocreate

urlpatterns = [
	url(r'^contratos/$', login_required(Contratos.as_view()), name='contratos'),
	url(r'^contrato/$', contratocreate, name='contrato'),
]
