from django import forms
from django.contrib.auth import authenticate

class AuthenticationForm(forms.Form):
	username=forms.CharField(label='Usuario')
	password=forms.CharField(label='Contrasena',widget=forms.PasswordInput)

	def __init__(self, *args,**kwargs):
		self.user_cache=None
		super(AuthenticationForm, self).__init__(*args, **kwargs)
		self.fields['username'].widget.attrs.update({'class' : 'form-control'})
		self.fields['username'].widget.attrs.update({'placeholder' : 'Usuario'})
		self.fields['password'].widget.attrs.update({'class' : 'form-control'})
		self.fields['password'].widget.attrs.update({'placeholder' : 'Constrasena'})
		
	def clean(self):
		username=self.cleaned_data.get('username')
		password=self.cleaned_data.get('password')
		self.user_cache=authenticate(username=username,password=password)

		if self.user_cache is None:
			raise forms.ValidationError('Ususario Incorrecto')
		elif not self.user_cache.is_active:
			raise forms.ValidationError('Usuario Inactivo')
		return self.cleaned_data

	def get_user(self):
		return self.user_cache
