from django.shortcuts import render
from django.contrib.auth import authenticate
from usuarios.forms import AuthenticationForm
from django.contrib.auth import login
from django.views.generic import FormView
from django.shortcuts import redirect
from django.views.generic import View
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.conf import settings
#from django.utils.decorators import method_decorator

# Create your views here.
class LoginView(FormView):
	form_class = AuthenticationForm
	template_name='login.html'
	success_url = '/'

	#@method_decorator(bitacora('Entrada'))
	def dispatch(self, *args, **kwargs):
		return super(LoginView, self).dispatch(*args, **kwargs)

	def form_valid(self, form):
		print 'entro'
		login(self.request, form.user_cache)
		return super(LoginView, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(LoginView, self).get_context_data(**kwargs)
		#context.update(data)
		return context

	def get(self, request, *args, **kwargs):
		if self.request.user.is_authenticated():
			return redirect('/')
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		context = self.get_context_data(**kwargs)
		context['form'] = form
		return self.render_to_response(context)

class LogoutView(View):
	def get(self, request):
		logout(request)
		return HttpResponseRedirect(settings.LOGIN_URL)
